var lexicon =  {"merry":"god",
				"christmas":"jul",
				"and":"och",
				"happy":"gott",
				"new":"nytt",
				"year":"år"};

function translate(text) {
	text = text.toLowerCase();
	var result = [],
	 	words = text.split(' ');
	for (var i = 0; i < words.length; i++) {
		result.push(lexicon[words[i]]);
	}
	return result.join(' ');
}


console.log(translate('merry christmas and happy new year'));