function sum(array) {
	return array.reduce(function  (previousValue, currentValue) {
		return previousValue + currentValue;
	});
}

function multiply(array) {
	return array.reduce(function  (previous, current) {
		return previous * current;
	});
}

console.log(sum([1,2,3,4]));
console.log(multiply([1,2,3,4]));
