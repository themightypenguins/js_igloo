function filterLongWords(array, length) {
 	var result = [];
 	for (var i = 0; i < array.length; i++) {
 		if (array[i].length > length)
 		{
 			result.push(array[i]);
 		}
 	}
 	return result;
 }

 console.log(filterLongWords(['123', '1', '12', '1234'], 2));