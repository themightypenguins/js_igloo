function isVowel(char) {

	if (char === 'a' ||
		char === 'e' ||
		char === 'i' ||
		char === 'u' ||
		char === 'o') {
		return true;
	}
	return false;
}

console.log(isVowel('a'));
console.log(isVowel('b'));
