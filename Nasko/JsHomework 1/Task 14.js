String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

function verbing(string) {
	if (string.length <= 2) {
		return string;
	}
	if (string.endsWith('ing')) {
		return string += 'ly';
	}
		return string + 'ing';
}

console.log(verbing('it'));
console.log(verbing('hit'));
console.log(verbing('bombing'));
