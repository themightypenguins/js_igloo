function fixStart(string) {
	var letter = string[0];
	var result = '' + letter;
	for (var i = 1; i < string.length; i++) {
		result += string[i] === letter ?  '*' : string[i];
	};
	return result;
}

console.log(fixStart('azmrazqa'));