function notBad(string) {
	var notIndex = string.indexOf('not');
	var badIndex = string.indexOf('bad');
	if (notIndex < badIndex) {
		return string.substr(0, notIndex) + 'good' + string.substr(badIndex + 3);
	}
	return string;
}

console.log(notBad('This dinner is not that bad!'));
console.log(notBad('It\'s bad yet not'));