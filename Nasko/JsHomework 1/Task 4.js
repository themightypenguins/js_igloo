function translate(text) {
	var translated = '';

	for (var i = 0; i < text.length; i++) {

		if (isVowel(text[i])) {
			translated += text[i];
		} else if (isLetter(text[i])) {
			translated += text[i];
			translated += 'o';
			translated += text[i];
		} else{ //this comes for whitespaces and other chars
			translated += text[i];
		}


		//this comes for whitespaces and other chars

	}
	return translated;
}

function isLetter(str) {
  return str.length === 1 && str.match(/[a-z]/i);
}

function isVowel(char) {

	if (char === 'a' ||
		char === 'e' ||
		char === 'i' ||
		char === 'u' ||
		char === 'o') {
		return true;
	}
	return false;
}

console.log(translate('this is fun'));