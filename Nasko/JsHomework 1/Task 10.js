function charFreq(string) {
	var result = {};

	for (var i = 0; i < string.length; i++) {
		if (!result[string[i]]) {
			result[string[i]] = 1;
		}
		else{
			result[string[i]]++;
		}
	}
	return result;
}

console.log(charFreq('1i'));