function randomizeOrder(array) {
	for (var i = 0; i < array.length; i++) {
		var item  = removeRandomItem(array);
		array.push(item);
	}
}

function removeRandomItem(array) {
	var randIndex = [Math.floor(Math.random()*array.length)],
		item = array.splice(randIndex,1)[0];
	// console.log(randIndex);
	// console.log(item);
	if (array.length === 0) {
		return null;
	}
	return item;
}

var har = [1,2,3,5,6,4];
randomizeOrder(har);
console.log(har);