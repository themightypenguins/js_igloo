function amount(num) {
	if (num >= 1000000) {
		return '$$$';
	}
	return num + ' leva';
}

console.log(amount(20));
console.log(amount(20000000));
