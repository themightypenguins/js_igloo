//Task 2
function maxOfThree(num1, num2, num3) {
    return Math.max(num1, num2, num3);
}

console.log('Task 2: '+ maxOfThree(3, 2, 1));

//Task 3
function isVowel(param) {
    var vowels = ['a', 'e', 'i', 'o', 'u'], 
        len = vowels.length, 
        isVowel;

    for (var i = 0; i < len; i++) {
        if (param === vowels[i]) {
            isVowel = true;
            break;
        } else {
            isVowel = false;
        }
    }

    return isVowel;
}

console.log('Task 3: ' + isVowel('a'));

//Task 4 -> not by me
function translate(text) {
    var result = [], vowels = 'aeoui';

    text.split('').forEach(
        function (character) {
            if (vowels.indexOf(character) === -1) {
                character = character + 'o' + character;
            }

            result.push(character);
        }
    );

    return result.join('');
}

console.log('Task 4: ' + translate('amnesia'));

//Task 5
function sum(arr) {
    var len = arr.length, sum = 0;

    for (var i = 0; i < len; i++) {
        sum = sum + arr[i];
    }

    return sum;
}

function multiply(arr) {
    var len = arr.length, product = 1;

    for (var i = 0; i < len; i++) {
        product = product * arr[i];
    }

    return product;
}

console.log('Task 5: Sum: ' + sum([1,2,3,4])+'; Product: ' + multiply([1,2,3]));

//Task 6
function reverse(str) {
    return str.split('').reverse().join('');
}

console.log('Task 6: Reverse: ' + reverse('makeshift'));

//Task 7
function translateToSwedish(str) {
    var dictionary = {
                        "merry":"god", 
                        "christmas":"jul",
                        "and":"och", 
                        "happy":"gott", 
                        "new":"nytt", 
                        "year":"år"
     }, 
        newStr = '';

    for (var i = 0; i < str.split(' ').length; i++) {
        for (var key in dictionary) {
            if (str.split(' ')[i] === key) {
                newStr += ' ' + dictionary[key];
            }
        }   
    }

    return newStr;
}

console.log('Task 7:'+ translateToSwedish('happy new year'));

//Task 8
function findLongestWord(words) {
    var len = words.length, longest = words[0];

    for (var i = 0; i < len; i++) {
        if (words[i].split('').length > longest.length) {
            longest = words[i];
        }
    }

    return longest;
}

console.log('Task 8: '+ findLongestWord(['ab', 'abcd', 'abc', 'abcdf']));

//Task 9
function filterLongWords(words, threshold) {
    var len = words.length, longest = [];

    for (var i = 0; i < len; i++) {
        if (words[i].split('').length > threshold) {
            longest.push(words[i]);
        }
    }

    return longest;
}

console.log('Task 9: '+ filterLongWords(['ab', 'abcd', 'abc', 'abcdf'], 3));

//Task 10
function charFreq(word) {
    var frequency = {};

    for (var i = 0; i < word.length; i++) {
        var character = word.charAt(i);
        if (frequency[character]) {
            frequency[character]++;
        } else {
            frequency[character] = 1;
        }
    }

    return frequency;
}

console.log('Task 10: '+ charFreq("abbabcbdbabdbdbabababcbcbab"));

//Task 11
function addCurrency(num) {
    if (num === 1000000) {
        num += ' ($$$)';
    } else {
        num += ' leva';
    }

    return num;
}

console.log('Task 11: '+ addCurrency(56));

//Task 12
function mixUp(word1, word2) {
    return word2.substring(0, 2)+word1.substring(2, word1.length)+' '+word1.substring(0, 2)+word2.substring(2, word1.length);
}

console.log('Task 12: '+ mixUp('yesterday', 'nope'));

//Task 13
function censorOccurance(word) {
    var firstChar = word.charAt(0), 
        censored = word.substring(1, word.length).split('');

    for (var i = 0; i < word.length; i++) {
        if (censored[i] === firstChar) {
            censored[i] = '*';
            break;
        }
    }

    return firstChar+censored.join('');
}

console.log('Task 13: '+ censorOccurance("aardvark"));

//Task 14
function ingOrLy(word) {
    var len = word.length;

    if (len >= 3) {
        if (word.substring(len-3, len) === 'ing') {
            word += 'ly';
        } else {
            word += 'ing';
        }
    } 

    return word;
}

console.log('Task 14: '+ ingOrLy('do'));

//Task 15
function muchNice(sentence) {
    if (sentence.indexOf('not') < sentence.indexOf('bad')) {
        sentence = sentence.substring(0, sentence.indexOf('not')) + 'good';
    }
    
    return sentence;
}

console.log('Task 15: '+ muchNice("This dinner is not that bad"));

//Task 16
function remove(arr, random) {
    //choose a radnom number for index from 0 to the length of the array
    var index = Math.floor(Math.random() * (arr.length - 1)), 
        word = arr.splice(index, 1);

    if (random === true) { //in case we want to mix up the words
        var index2 = 0;
        while (index === index2) {
            index2 = Math.floor(Math.random() * (arr.length - 1));
        }
        arr.splice(index2, 0, word.join());
    } 

    return arr;
}

console.log('Task 16: '+ remove(['blah', 'meh', 'leh', 'ew', 'nope']));

//Task 17
function reorder(arr) {
    return remove(arr, true);
}

console.log('Task 17: '+ reorder(['blah', 'meh', 'leh', 'ew', 'nope']));