var ajaxRequest,
    jsonObj,
    months = {
        'Jan' : 1,
        'Feb' : 2,
        'Mar' : 3,
        'Apr' : 4,
        'May' : 5,
        'Jun' : 6,
        'Jul' : 7,
        'Aug' : 8,
        'Sep' : 9,
        'Oct' : 10,
        'Nov' : 11,
        'Dec' : 12
    };

if (window.XMLHttpRequest) {
    ajaxRequest = new XMLHttpRequest();
}

ajaxRequest.onreadystatechange = function () {
    if (ajaxRequest.readyState === 4 ) {
        if (ajaxRequest.status === 200 && ajaxRequest.status < 300) {
            jsonObj = JSON.parse(ajaxRequest.responseText);
            document.body.innerHTML += makeTable(jsonObj);
        }
    }
};

ajaxRequest.open("GET", "https://api.myjson.com/bins/356at", true);
ajaxRequest.send();

/***
 * Composes a table from a json file
 * @param {Array} jsonObj
 * returns {String} - html
 */
function makeTable(jsonObj) {
    var html = '<table class="table table-bordered" id="data"><thead><tr>';

    html += getProperties(jsonObj[0]);
    html += '</tr></thead><tbody>';

    jsonObj.forEach(function (obj) {
        html += '<tr>';
        html += makeCells(obj);
        html += '</tr>';
    });

    html += '</tbody></table>';
    return html;
}

/***
 * Fetches properies from an object
 * @param {Object} obj
 * returns {String} - html
 */
function getProperties(obj) {
    var html = '',
        property;

    for (property in obj) {
        html += '<th>'+property+'</th>';
    }

    return html;
}

/***
 * Compiles table cells
 * @param {Object} obj
 * returns {String} - html
 */
function makeCells(obj) {
    var html = '',
        property;

    for (property in obj) {
        if (typeof obj[property] === 'object' && typeof obj[property] !== null) {
            html += '<td>';
            html += '<table class="table table-bordered"><thead><tr>';
            html += getProperties(obj[property]);
            html += '</tr></thead><tbody><tr>';
            html += makeCells(obj[property]);
            html += '</tbody></table>';
            html += '</td>';
        } else {
            html += '<td>'+obj[property]+'</td>';
        }
    }

    return html;
}

/***
 * Sorts table by given property
 * @param {String} columnName
 * returns void
 */
function sortBy(columnName) {
    if (jsonObj[0].hasOwnProperty(columnName)) {
        jsonObj.sort(function (a, b) {
            if (!isNaN(a[columnName])) {
                return a[columnName] - b[columnName];
            } else {
                if (months.hasOwnProperty(a[columnName].substr(0,3))) {
                    return sortByDate(a, b, columnName);
                } else {
                    return a[columnName].localeCompare(b[columnName]);
                }
            }     
        });  
    }
     
    document.getElementById('data').innerHTML = makeTable(jsonObj);
}


/***
 * Determines how the table should be sorted by date
 * @params {Object} a, {Object} b, {String} columnName
 * returns {Number}
 */
function sortByDate(a, b, columnName) {
        if (a[columnName].substr(0,3) === b[columnName].substr(0,3)) {
            var tempA = a[columnName].substr(4,2);
            var tempB = b[columnName].substr(4,2);

            if (isNaN(a[columnName].charAt(5))) {
                tempA = a[columnName].charAt(4);
            }

            if (isNaN(b[columnName].charAt(5))) {
                tempB = b[columnName].charAt(4);
            }

            return parseInt(tempA) - parseInt(tempB);

        } else {
            return parseInt(months[a[columnName].substr(0,3)]) - parseInt(months[b[columnName].substr(0,3)]);       
        }
}
