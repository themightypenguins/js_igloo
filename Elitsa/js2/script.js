function createTable() {
    var table = document.createElement("table");
    table.classList.add('table');
    table.classList.add('table-bordered');
    table.classList.add('table-hover');
    return table;
}

function addTable(parent, data) {
	var table = createTable();

    addHeader(table, data[0]);
	for (var i = 0; i < data.length; i++) {
		addRow(table, data[i]);
	}

    parent.appendChild(table);
}

function addSubTable(parent, obj) {
    var table = createTable();

    addHeader(table, obj);
    addValues(table, obj);

    parent.appendChild(table);
}

function addHeader(table, obj) {
    var header = table.createTHead(),
        row = header.insertRow();
    for (var p in obj) {
        var th = header.children[0].appendChild(document.createElement("th"))
        th.innerHTML = p;
    }
}

function addValues(table, obj) {

    var row = table.insertRow();

    for (var p in obj) {

        var cell = row.insertCell(),
            val = obj[p];

        if (typeof val === 'object') {
            addSubTable(cell, val);
        } else {
            cell.innerHTML = val;
        }
    }
}

function addRow(table, data) {
	var row = table.insertRow();

    for (var key in data) {
        addCell(row, data[key]);
    }
}

function addCell(row, val) {

	var cell = row.insertCell();

	if (typeof val === 'object' && val !== null) {
        addSubTable(cell, val);
    } else {
        cell.innerHTML = val;
    }
}

function fetchJson() {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var data = JSON.parse(xmlhttp.responseText);
            console.time('total');
            addTable(document.body, data);
            console.timeEnd('total');
        }
    };

    xmlhttp.open("GET", "data.json", true); // or use https://api.myjson.com/bins/1r3r5
    xmlhttp.send();
}

fetchJson();