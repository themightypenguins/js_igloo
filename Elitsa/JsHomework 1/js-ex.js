// Task2

var maxOfThree = function(a, b, c) {
    return Math.max(a, b, c);
};

// Task3

var phrase = function(phrase) {
    var vowels = ["a", "e", "i", "o", "u"];
    for (var i = 0; i <= vowels.lenght; i++) {
        if (phrase === vowels[i]) {
            return true;
        } else {
            return false;
        }
    }
};

// Task4

var translate = function(string) {
    var result = [],
        vowels = 'aeoui';
    string.split('').forEach(function(character) {
        if (vowels.indexOf(character) === -1 && character.test('[/a-z/i]')) {
            character = character + 'o' + character;
        }
        result.push(character);
    });
    return result.join('');

};

// Task5

function sum() {
    var total = 0,
        numbers = [1, 2, 3, 4];
    for (var i = 0; i < numbers.length; i++) {
        total += numbers[i];
    }
    return total;
}

function multiplies() {
    var total = 0,
        numbers = [1, 2, 3, 4];
    for (var i = 0; i < numbers.length; i++) {
        total = total * numbers[i];
    }
    return total;
}

// Task6

var reverse = function (string) {
    var length = string.length,
        reversed = [],
        joined = "";
    for (var i = length; i > 0; i--) {
        reversed.push(string.charAt(i - 1));
    }
    for (var j = 0; j < (length); j++) {
        joined += (reversed[j]);
    }
    return joined;
};

// Task7

var translate = function (string) {
    var stringArray = string.split(" "),
        translate = {
            "merry": "god",
            "christmas": "jul",
            "and": "och",
            "happy": "gott",
            "new": "nytt",
            "year": "år"
        },
        translation = "";
    stringArray.forEach(function(element) {
        translation += translate[element] + " ";
    });
    return translation;
};

// Task8

var findLongestWord = function (array) {
    var elements = array.length,
        count = 0;
    for (var i = 0; i < elements; i++) {
        if (array[i].length > count)
        count = array[i].length;
      }
  return count;
};

// Task9

var filterLongWords = function (array, int) {
    var length = array.length,
        longestWords = [];
    for (var i = 0; i < length; i++) {
        if (array[i].length > int) {
            longestWords[longestWords.length] = array[i];
        }
    }
    return longestWords;
};

//Task10
var charFreq = function (string) {
    var list = {},
        length = string.length;
    for (var i = 0; i < length; i++) {
        if (string.charAt(i) in list)
            list[string.charAt(i)] += +1;
        else
            list[string.charAt(i)] = 1;
      }
    return list;
};