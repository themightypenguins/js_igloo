// variable declariation
        var out = "",
            xmlhttp = new XMLHttpRequest(),
            url = 'test.json';

        // Ajax
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var myArr = JSON.parse(xmlhttp.responseText);
                //  gets all the objects in the JSON and for each of them runs myFunction
                out+='<table>';
                printproperties(myArr[0]);
                for (var i = 0; i < myArr.length; i++) {
                    myFunction(myArr[i]);
                }
                out+='</table>';
                document.getElementById("jsonOutput").innerHTML = out;
            }
        };
        xmlhttp.open('GET', url, true);
        xmlhttp.send();


        function printproperties(myArr){
            out+='<tr>';
            for (var t in myArr) {
                out+= '<td>'+ t +'</td>';// prints the array properties
            }
            out+='</tr>';
        };
        function myFunction(myArr){


            out += '<tr>';
            for (var t in myArr) {
                if(typeof myArr[t] === 'object' &&  myArr[t]!==null){
                    out+='<td>';
                    out+='<table>';
                    printproperties(myArr[t]);
                    myFunction(myArr[t]); // recursive calls the function
                    out+='</table>';
                    out+='</td>';
                } else{
                    out+='<td>'+ myArr[t]+'</td>'; // prints the array values
                }
            }
            out += '</tr>';

        };

