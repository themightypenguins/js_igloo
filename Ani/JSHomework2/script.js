// variable declariation
        var out = "",
            xmlhttp = new XMLHttpRequest(),
            url = 'test.json';

        // Ajax
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var myArr = JSON.parse(xmlhttp.responseText);
                //  gets all the objects in the JSON and for each of them runs myFunction
                for (var i = 0; i < myArr.length; i++) {
                    myFunction(myArr[i]);
                }
                document.getElementById("jsonOutput").innerHTML = out;
            }
        };
        xmlhttp.open('GET', url, true);
        xmlhttp.send();

        // myFunction get the array and returns out (html output)
        function myFunction(myArr) {
                out+= '<table>';
                for (var t in myArr) {
                    out+= '<tr><td>'+ t +'</td>'; // prints the array properties
                    if(typeof myArr[t] === 'object' &&  myArr[t]!==null){
                        out+='<td>';
                        myFunction(myArr[t]); // recursive calls the function
                        out+='</td>';
                    } else{
                        out+='<td>'+ myArr[t]+'</td>'; // prints the array values
                    }
                    out += '</tr>';
                }
                out+='</table>';
            return out;
        }
