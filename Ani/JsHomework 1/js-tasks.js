// Task1 :
function maxi(n1,n2){
    var maxy= Math.max(n1,n2);
    return maxy;
}
console.log(maxi(5,6));
//-------------------------------------------------------------//

// Task2 :
function maxOfThree(n1,n2,n3){
    var max= Math.max(n1,n2,n3);
    return max;
}
console.log(maxOfThree(2,6,8));
//-------------------------------------------------------------//

// Task3 :
function isVowel(letter){
    var vowels = ["a", "e", "i", "o", "u"];
    if(letter.length==1){
        for(var i = 0; i < vowels.length; i++){
            if(letter === vowels[i]){
                return true;
             }
        }
        return false;
    }
}
console.log(isVowel("g"));
//-------------------------------------------------------------//

// Task4 :
function task4 (text){
    var vowels = ["a","e","i","o","u"],
        splitter= text.split(" ");
        text="";
        for (var i = 0; i < splitter.length; i++) {
            var letters= splitter[i].split("");
            for (var j = 0; j < letters.length; j++) {
                if(letters[j] === vowels[0]){
                    text=text+letters[j];
                } else if(letters[j] === vowels[1]){
                    text=text+letters[j];
                } else if(letters[j] === vowels[2]){
                    text=text+letters[j];
                } else if(letters[j] === vowels[3]){
                    text=text+letters[j];
                } else if(letters[j] === vowels[4]){
                    text=text+letters[j];
                } else {
                    text=text+letters[j]+"o"+letters[j];
                }
            }
            text= text+ " ";
        }
        return text;
}
console.log(task4("this is fun"));
//-------------------------------------------------------------//

// Task5 :
function sum() {
    var result=0;
    for (var i = 0; i < arguments.length; i++) {
    result=result+arguments[i];
    }
    return result;
}
console.log(sum(1,2,3,4));

function multiply(){
    var result=1;
    for (var i = 0; i < arguments.length; i++) {
    result=result*arguments[i];
    }
    return result;
}
console.log(multiply(1,2,3,4));
//-------------------------------------------------------------//

// Task6 :
function reverse(s){
    return s.split("").reverse().join("");
}
console.log(reverse("jag testar"));
//-------------------------------------------------------------//

// Task7 :
function translate(text){
    var obj={
        "merry":"god",
        "christmas":"jul",
        "and":"och",
        "happy":"gott",
        "new":"nytt",
        "year":"år"
    },
    result="",
    string=text.split(" ");
    for (var i = 0; i < string.length; i++) {
        for(var key in obj){
            if(obj.hasOwnProperty(key) && key===string[i]){
                result=result+obj[key]+" ";
            }
        }
    }
    return result;
}
console.log(translate("merry christmas"));
//-------------------------------------------------------------//

// Task8 :
function findLongestWord(){
    var longest="",
        lenght=0;
    for (var i = 0; i < arguments.length; i++) {
        if(arguments[i].length > lenght){
            lenght = arguments[i].length;
            longest = arguments[i];
        }
    }
    return longest;
}
console.log(findLongestWord("eryh","reyeyurey","yery","uyk","sa"));
//-------------------------------------------------------------//

// Task9 :
function filterLongWords(array,int){
    var words=[];
    for (var i = 0; i < array.length; i++) {
        if(array[i].length > int){
            words.push(array[i]);
        }
    }
    return words;
}
console.log(filterLongWords(["dsfs","asf","safasfsf","aQEE"],3));
//-------------------------------------------------------------//

// Task10 :
function charFreq(string){
    var list={};
    for (var i = 0; i < string.length; i++) {
        if (string.charAt(i) in list)
            list[string.charAt(i)] += +1;
        else
            list[string.charAt(i)] = 1;
          }
        return list;
    }
console.log(charFreq("sfafaaa"));
//-------------------------------------------------------------//

// Task11 :
function chekValue(num){
    if (num===1000000) {
        console.log("Result: "+num+" dollars ($$$)");
    } else {
        console.log("Result: "+num+" leva");
    }
}
chekValue(120);
chekValue(1000000);
//-------------------------------------------------------------//

// Task12 :
function mixUp(str1,str2){
    var result="Result: ";
    if (str1.length>2 && str2.length>2) {
        result=result+str2.substring(0,2)+str1.substring(2,str1.length)+" "+str1.substring(0,2)+str2.substring(2,str2.length);
    }
    return result;
}
console.log(mixUp("sad","puppy"));
//-------------------------------------------------------------//

// Task13 :
function fixStart(str){
    var string=str.split(""),
        letter=string[0],
        counter=1;
    for (var i = 1; i < string.length; i++) {
        if (counter!=2 && string[i]===letter){
            string[i]="*";
            counter=2;
        }
    }
    return string.join("");
}
console.log(fixStart("google"));
//-------------------------------------------------------------//

// Task14 :
function verbing(verb){
    if(verb.length>3){
        if (verb.indexOf("ing") !=-1) {
            return verb+"ly";
        } else if(verb.indexOf("ing")){
            return verb+"ing";}

        } else {
            return verb;
        }
}
console.log(verbing("swimm"));
//-------------------------------------------------------------//

// Task15 :
function notBad(text){
    if (text.indexOf("not")<text.indexOf("bad")) {
        return text.substring(0,text.indexOf("not")-1)+" good"+text.substring(text.indexOf("bad")+3,text.length);
    } else{
        return text;
    }
}
console.log(notBad("This dinner is not that bad!"));
//-------------------------------------------------------------//

// Task16 :
function removeRandomItem(array){
    function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;}
    var randomNumber=getRandomInt(0,array.length-1);
    var randomItem=array[randomNumber];
    array.splice(randomNumber, 1);
    return "Result: "+ randomItem +" / leaving: "+ array;
}
console.log(removeRandomItem(["This","dinner","is","not","that","bad"]));
//------------------------------------------------------------------------//

// Task17 :
function shuffle(array) {
  var currentIndex = array.length,
      temporaryValue,
      randomIndex ;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
console.log(shuffle(["This","dinner","is","not","that","bad"]));
//-------------------------------------------------------------------//
